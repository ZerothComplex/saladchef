﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerTimeAnimation : MonoBehaviour
{
    [SerializeField]
    private Transform m_Model;

    // Update is called once per frame
    void Update()
    {
        m_Model.transform.Rotate(Vector3.up);
    }
}
