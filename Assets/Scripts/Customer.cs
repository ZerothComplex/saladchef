﻿using UnityEngine;

public class Customer : MonoBehaviour
{
    public class Emotions
    {
        public bool isAngry;
        public bool isPlayer1;
    }

    [SerializeField] private UICustomerState m_State;

    public float currentWaitTime { get; private set; }
    public float totalWaitTime { get; private set; }

    public Emotions emotions { get; private set; } = new Emotions();

    //Customer's order
    private Salad _order;

    //Current wait time factor
    //Increases on based of cusomter order received
    private float _waitTimeFactor = 1f;

    private void Start()
    {
        Generate();
    }

    private void Update()
    {
        m_State.SetTime(currentWaitTime, totalWaitTime);
        currentWaitTime -= Time.deltaTime * _waitTimeFactor;

        if (Mathf.RoundToInt(currentWaitTime) == 0)
            Leave();
    }

    /// <summary>
    /// Leave and make way for new age... i mean customer
    /// </summary>
    private void Leave()
    {
        ScoreController.Instance.OnCustomerUnfullfilledRequest(emotions.isAngry, emotions.isPlayer1);
        Generate();
    }

    /// <summary>
    /// Verify's order received from chep to the customer's order
    /// </summary>
    /// <param name="order">chef's salad</param>
    /// <returns></returns>
    public bool VerifyOrder(Salad order, bool isPlayer1)
    {
        if (_order == order)
        {
            Generate();
            return true;
        }
        else
        {
            emotions.isPlayer1 = isPlayer1;
            emotions.isAngry = true;
            _waitTimeFactor = 2f;
            return false;
        }
    }

    /// <summary>
    /// Generates a salad with random combination of vegetables
    /// Resets the state of customer, acting as customer generator
    /// </summary>
    public void Generate()
    {
        _order = Salad.GenerateRandom();
        m_State.SetOrder(_order.ToString());
        currentWaitTime = Random.Range(Constants.CUSTOMER_WAIT_TIME_FACTOR,
                                       _order.vegetabeCount * (Constants.CUSTOMER_WAIT_TIME_FACTOR + 1));
        totalWaitTime = currentWaitTime;
        _waitTimeFactor = 1f;
    }

}
