﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerGenerator : Singleton<PowerGenerator>
{
    [SerializeField] private GameObject m_SpeedPrefab;
    [SerializeField] private GameObject m_TimePrefab;
    [SerializeField] private GameObject m_ScorePrefab;


    public void Generate()
    {
        Power.Type type = (Power.Type)Random.Range(0, 3);

        switch (type)
        {
            case Power.Type.speed:
                GameObject speedPower = Instantiate(m_SpeedPrefab);
                break;
            case Power.Type.time:
                GameObject timePower = Instantiate(m_TimePrefab);
                break;
            case Power.Type.score:
                GameObject scorePower = Instantiate(m_ScorePrefab);
                break;
            default:
                break;
        }
    }
}
