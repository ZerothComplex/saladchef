﻿using UnityEngine;

public class ScoreController : Singleton<ScoreController>
{
    private float _player1Score = 0;
    private float _player2Score = 0;

    public void CalculateScore(bool isPlayer1, Salad salad, Customer customer)
    {
        float currentWaitTime = customer.currentWaitTime;
        float totalWaitTime = customer.totalWaitTime;

        if (customer.VerifyOrder(salad, isPlayer1))
        {
            if (currentWaitTime / totalWaitTime > 0.70)
            {
                //Generates power for player
                PowerGenerator.Instance.Generate();
            }

            if (isPlayer1)
                _player1Score += 30;
            else
                _player2Score += 30;
        }
        else
        {
            if (isPlayer1)
                _player1Score -= 30;
            else
                _player2Score -= 30;
        }

        UIManager.Instance.UpdateScore(_player1Score.ToString(), isPlayer1);
    }

    /// <summary>
    /// Update score when customer left without receiving order
    /// Decrease both player's score
    /// </summary>
    public void OnCustomerUnfullfilledRequest(bool isAngry, bool isPlayer1)
    {
        if (isAngry)
        {
            if (isPlayer1)
            {
                _player1Score -= 60;
                UIManager.Instance.UpdateScore(_player1Score.ToString(), true);
            }
            else
            {
                _player2Score -= 60;
                UIManager.Instance.UpdateScore(_player2Score.ToString(), false);
            }
        }
        else
        {
            _player1Score -= 30;
            _player2Score -= 30;
            UIManager.Instance.UpdateScore(_player1Score.ToString(), true);
            UIManager.Instance.UpdateScore(_player2Score.ToString(), false);
        }
    }

    public int GetWinner()
    {
        int winner = 0;

        if (_player1Score > _player2Score)
        {
            winner = 1;
            HighScore.Add(Mathf.RoundToInt(_player1Score), winner);
        }
        else if (_player1Score < _player2Score)
        {
            winner = 2;
            HighScore.Add(Mathf.RoundToInt(_player2Score), winner);
        }
        else
        {
            winner = 0;
            HighScore.Add(Mathf.RoundToInt(_player1Score), winner);
        }


        return winner;
    }

    /// <summary>
    /// Adds the points to player score
    /// </summary>
    /// <param name="isPlayer1">If set to <c>true</c> is player1.</param>
    /// <param name="points">Points.</param>
    public void AddPoints(bool isPlayer1, int points)
    {
        if(isPlayer1)
        {
            _player1Score += points;
            UIManager.Instance.UpdateScore(_player1Score.ToString(), true);
        }
        else
        {
            _player2Score += points;
            UIManager.Instance.UpdateScore(_player2Score.ToString(), true);
        }
    }
}
