﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.ObjectModel;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private bool m_IsPlayer1 = true;
    [SerializeField] private float m_MoveSpeed = 7;
    [SerializeField] private ObservableCollection<Vegetable> m_Vegetables = new ObservableCollection<Vegetable>();
    [SerializeField] private float m_ChopSpeed = 2f;
    [SerializeField] private GameObject m_Status;
    [SerializeField] private int m_MaxVegetableCapacity = 2;


    private Vegetable _currentVegetable { get; set; }
    private Vegetable _vegetableOnPlate { get; set; }
    private Salad _salad = new Salad();
    private Rigidbody _rigidBody = null;
    private Customer _currentCustomer = null;
    private bool _canMove = true;

    enum State
    {
        none,
        pick,   //Pick vegetable
        chop,   //Chop vegetable on chopping board
        plate,  //Put a vegetable on table
        serve,  //Serve salad to a customer
        trash   //Throw salad away
    }

    private State _state;

    private void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();

        //To ignore player 1 and 2 collision
        Physics.IgnoreLayerCollision(9, 9);

        m_Vegetables.CollectionChanged += OnVegetablesChanged;
    }

    private void OnVegetablesChanged(object sender,
                                     System.Collections.Specialized.NotifyCollectionChangedEventArgs e) => SetCarriedVegetableText();

    private void Update()
    {
        if (_canMove)
            UpdatePlayerMovement();

        //Show player status
        //What vegetable is player carrying
        if (m_Status != null)
        {
            float x = transform.position.x + 1;
            float y = m_Status.transform.position.y;
            float z = transform.position.z + 1;

            m_Status.transform.position = new Vector3(x, y, z);
        }
        else
            Debug.LogWarning("Player status UI element not assigned");
    }

    #region PlayerMovement

    private void UpdatePlayerMovement()
    {
        //Depending on which player, use different input for moving
        if (m_IsPlayer1)
            UpdatePlayer1Movement();
        else
            UpdatePlayer2Movement();
    }

    /// <summary>
    /// Updates Player1's movement using the WASD keys 
    /// </summary>
    private void UpdatePlayer1Movement()
    {
        if (Input.GetKey(KeyCode.W))
        {
            //Up movement
            _rigidBody.velocity = Vector3.forward * m_MoveSpeed;
        }

        if (Input.GetKey(KeyCode.A))
        {
            //Left movement
            _rigidBody.velocity = Vector3.left * m_MoveSpeed;
        }

        if (Input.GetKey(KeyCode.S))
        {
            //Down movement
            _rigidBody.velocity = Vector3.back * m_MoveSpeed;
        }

        if (Input.GetKey(KeyCode.D))
        {
            //Right movement
            _rigidBody.velocity = Vector3.right * m_MoveSpeed;
        }

        if (Input.GetKeyDown(KeyCode.E))
            OnActions();
    }

    /// <summary>
    /// Updates Player2's movement using the arrow keys 
    /// </summary>
    private void UpdatePlayer2Movement()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            //Up movement
            _rigidBody.velocity = Vector3.forward * m_MoveSpeed;
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            //Left movement
            _rigidBody.velocity = Vector3.left * m_MoveSpeed;
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            //Down movement
            _rigidBody.velocity = Vector3.back * m_MoveSpeed;
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            //Right movement
            _rigidBody.velocity = Vector3.right * m_MoveSpeed;
        }

        if (Input.GetKeyDown(KeyCode.KeypadEnter))
            OnActions();
    }

    #endregion

    #region Triggers
    //OnTriggerStay is called once per physics update for every collider that is touching the trigger
    private void OnTriggerStay(Collider collider)
    {
        //Coutinous check for whether the player is hitting a vegetable or board
        switch (collider.tag)
        {
            case Constants.TAG_VEGETABLE:
                string vegetableName = collider.name.Split('-')[1];
                _currentVegetable = new Vegetable(vegetableName);
                _state = State.pick;
                break;
            case Constants.TAG_BOARD:
                if ((collider.name.Contains("1") && m_IsPlayer1) ||
                 (collider.name.Contains("2") && !m_IsPlayer1))
                    _state = State.chop;
                break;
            case Constants.TAG_PLATE:
                _state = State.plate;
                break;
            case Constants.TAG_TRASHCAN:
                _state = State.trash;
                break;
            case Constants.TAG_CUSTOMER:
                _currentCustomer = collider.GetComponent<Customer>();
                _state = State.serve;
                break;
            case Constants.TAG_POWER:
                Power power = collider.GetComponent<Power>();
                AddPower(power);
                break;
            default:
                break;


        }
    }

    private void OnTriggerExit(Collider collider)
    {
        //Player have moved away from vegetable
        switch (collider.tag)
        {
            case Constants.TAG_VEGETABLE:
                _state = State.none;
                _currentVegetable = null;
                break;
            case Constants.TAG_BOARD:
            case Constants.TAG_PLATE:
            case Constants.TAG_TRASHCAN:
            case Constants.TAG_CUSTOMER:
                _state = State.none;
                _currentCustomer = null;
                break;
            default:
                break;
        }


    }
    #endregion

    #region Actions

    private void OnActions()
    {
        switch (_state)
        {
            case State.none:
                break;
            case State.pick:
                PickVegetable();
                break;
            case State.chop:
                ChopVegetables();
                break;
            case State.plate:
                PutVegetableOnPlate();
                break;
            case State.serve:
                ServeToCustomer();
                break;
            case State.trash:
                DumpVegetables();
                break;
            default:
                break;
        }
    }


    private void AddVegetable()
    {
        Vegetable vegetable = m_Vegetables[1];
        m_Vegetables[1] = _currentVegetable;
        m_Vegetables[0] = vegetable;
    }

    private void UpdatePlayerVegetableStatus()
    {
        string vegetables = "";

        foreach (Vegetable vegetable in m_Vegetables)
            vegetables += vegetable.name + ",";

        if (vegetables.Length == 0)
            vegetables = "...";
        else
            vegetables = vegetables.Remove(vegetables.Length - 1);

        UIManager.Instance.UpdatePlayerStatus(vegetables, m_IsPlayer1);
    }

    /// <summary>
    /// Picks the currently colliding vegetable adds to vegetable list
    /// </summary>
    private void PickVegetable()
    {
        if (_currentVegetable == null)
            return;

        if (m_Vegetables.Count == m_MaxVegetableCapacity)
        {
            Vegetable vegetable = m_Vegetables[1];
            m_Vegetables[1] = _currentVegetable;
            m_Vegetables[0] = vegetable;
        }
        else if (m_Vegetables.Count < m_MaxVegetableCapacity)
        {
            m_Vegetables.Add(_currentVegetable);
        }
    }

    /// <summary>
    /// Chops a single vegetable in given time and adds it to salad
    /// </summary>
    private void ChopVegetables()
    {
        if (m_Vegetables.Count == 0)
        {
            if (_salad.isEmpty)
                return;

            PickSalad();
            return;
        }

        StartCoroutine(ChopVegtableRoutine());
    }

    private IEnumerator ChopVegtableRoutine()
    {
        //Pause player movement and interaction until vegetable isn't chopped
        _canMove = false;
        //Wait for chopping of vegetable
        yield return new WaitForSeconds(m_ChopSpeed);
        //Chopping of a vegetable is complete
        Vegetable vegetable = m_Vegetables.First();
        //Add to salad
        _salad.Add(vegetable);
        //Remove from currently holding vegetables
        m_Vegetables.Remove(vegetable);
        //Show salad on board status
        UIManager.Instance.UpdateChoppingBoardStatus(_salad.ToString(), m_IsPlayer1);
        //Resume player movement
        _canMove = true;
    }

    private void SetCarriedVegetableText()
    {
        if (m_Vegetables.Count == 0)
        {
            UIManager.Instance.UpdatePlayerStatus("...", m_IsPlayer1);
            return;
        }

        UpdatePlayerVegetableStatus();
    }

    private void PutVegetableOnPlate()
    {
        //Put vegetable on table if no vegetable on table and player have vegetable
        if (_vegetableOnPlate == null && m_Vegetables.Count != 0)
        {
            Vegetable vegetable = m_Vegetables.First();
            _vegetableOnPlate = vegetable;
            UIManager.Instance.UpdatePlateStatus(_vegetableOnPlate.name, m_IsPlayer1);
            m_Vegetables.Remove(vegetable);
            UpdatePlayerVegetableStatus();
        }
        //Pick vegetable from plate if there is vegetable on plate and player have lesser vegetable tham max capacity
        else if (_vegetableOnPlate != null && m_Vegetables.Count < m_MaxVegetableCapacity)
        {
            m_Vegetables.Add(_vegetableOnPlate);
            _vegetableOnPlate = null;
            UIManager.Instance.UpdatePlateStatus("...", m_IsPlayer1);
            UpdatePlayerVegetableStatus();
        }
    }
    /// <summary>
    /// Pick salad from chopping board after chopping vegetable
    /// </summary>
    private void PickSalad()
    {
        _salad.isPickedUp = true;
        UIManager.Instance.UpdatePlayerStatus(_salad.ToString(), m_IsPlayer1);
        UIManager.Instance.UpdateChoppingBoardStatus("...", m_IsPlayer1);
    }

    /// <summary>
    /// Throw vegetable in trash
    /// </summary>
    private void DumpVegetables()
    {
        if (_salad.isEmpty || !_salad.isPickedUp)
            return;

        UIManager.Instance.UpdatePlayerStatus("...", m_IsPlayer1);
        _salad.Clear();
    }

    /// <summary>
    /// Serve salad to customer
    /// </summary>
    private void ServeToCustomer()
    {
        if (_salad.isEmpty)
            return;

        ScoreController.Instance.CalculateScore(m_IsPlayer1, _salad, _currentCustomer);
        UIManager.Instance.UpdatePlayerStatus("...", m_IsPlayer1);
        _salad.isPickedUp = false;
        _salad.Clear();
    }

    #endregion

    /// <summary>
    /// Gives power to player
    /// </summary>
    private void AddPower(Power power)
    {
        switch (power.type)
        {
            case Power.Type.speed:
                m_MoveSpeed = 10;
                break;
            case Power.Type.score:
                ScoreController.Instance.AddPoints(m_IsPlayer1, 50);
                break;
            case Power.Type.time:
                GameManager.Instance.AddTime(m_IsPlayer1, 50);
                break;
            default:
                break;
        }

        Destroy(power.gameObject);
    }
}
