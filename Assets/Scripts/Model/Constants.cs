﻿
public class Constants
{
    //Types of vegetables
    public const string VEGETABLE_A = "A";
    public const string VEGETABLE_B = "B";
    public const string VEGETABLE_C = "C";

    public const float DEFAULT_TIME = 120;
    public const float CUSTOMER_WAIT_TIME_FACTOR = 15;

    //Tags
    public const string TAG_VEGETABLE = "Vegetable";
    public const string TAG_BOARD = "Board";
    public const string TAG_PLATE = "Plate";
    public const string TAG_TRASHCAN = "Trashcan";
    public const string TAG_CUSTOMER = "Customer";
    public const string TAG_POWER = "Power";

}
