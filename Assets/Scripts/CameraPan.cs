﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraPan : MonoBehaviour
{
    [SerializeField] private GameObject m_Player1;
    [SerializeField] private GameObject m_Player2;
    [SerializeField] private float m_MinimumZoom = 8;
    [SerializeField] private float m_MaximumZoom = 12;

    private float _distance;
    private float _value;
    private Camera _camera;

    private void Start()
    {
        _camera = GetComponent<Camera>();
    }

    private void Update()
    {
        //Get the distance between two players
        _distance = Vector3.Distance(m_Player1.transform.position, m_Player2.transform.position);

        //Clamp the value
        _value = Mathf.Clamp(_distance, m_MinimumZoom, m_MaximumZoom);

        //Set the orthographic size to lerp
        _camera.orthographicSize = Mathf.Lerp(_camera.orthographicSize, _value, Time.deltaTime);

    }
}
