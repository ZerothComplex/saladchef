# Salad Chef

A salad chef simulation game made in Unity. Top-down chef battle game where two players fight each other in making the most customer happy by completing their's salad orders.
Most 3D Models in the game is made as prototype placeholder version using probuilder. 

## Requirements

Unity 2018.3+

## How to Play

- Pick a vegetable from letter counter(Can pick 2 at a time).
- Put it on chopping board to cut.
- Can put one vegetable on the chef's plate.
- Add vegetables to the chopping board to make a new salad combination.
- Serve salad with the correct combination of vegetables to a customer with the customer wait time.
- Gain points for correct combination.
- Customer will become angry with the wrong combination, and wait time will decrease faster.
- A customer leaving will give both players negative points.
- An angry customer will give double the negative points to the player who made the customer angry.
- The game ends when the time of both players ends.
- The player with a max score of the two wins.


### Powers
- Speed - Increases speed of player.
- Time - Increases time of player. 
- Points - Gives bonus points to the player.  

### Controls 
#### Player 1 
- [WASD] Movement 
- [E] Action 
#### Player 2 
- [WASD] Movement 
- [Keypad Enter] Action

## Future

- Add tutorial and help.
- Add better UI and more menu options.
- Add support for more than 2 player.
- Add a config class to support the whole config of the game.
- Add multiplayer support.
- Add cross-platform support.
- Add joystick support.
- Add asset bundles to make (models, texture, sounds) interchangeable.


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


